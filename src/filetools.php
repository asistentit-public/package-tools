<?
namespace asistentit\tools;

class filetools
{
    public static function GET_FILES_FROM($path, $filter = '*.php', $keyisfilename = true, $recusion = true)
	{
		$FILES = array();

		foreach ((array) glob($path . $filter) as $filename)
		{
            $key = ($keyisfilename) ? basename($filename) : UUID::v4();
			if (file_exists($filename) && file_exists($filename)) $FILES[$key] = $filename;
		}
        
        if ($recusion)
        {
            foreach ((array) glob($path . '*') as $filename)
            {
                if (!is_dir($filename)) continue;
                $FILES = array_merge(self::GET_FILES_FROM($filename, $filter, $keyisfilename, $recusion));
            }
        }

		return $FILES;
    }
    
    public static function DELETE_DIR($dir)
	{
		if (!file_exists($dir) || !is_dir($dir)) return;
		foreach(glob($dir . '/*') as $file) 
		{
			if (!in_array(basename($file), array('.','..')))
			{
				if(is_dir($file))
					self::DELETE_DIR($file);
				else
				{
					try 
					{ 
						unlink($file); 
					} catch(Exception $e)
					{
						
					}
				}
			}
		}
		try
		{
			rmdir($dir);
		} catch(Exception $e)
		{
			
		}
    }

    public static function COUNT_FILE_FROM_DIR($dir, $tips = array('f'))
	{
		$tips = (array)$tips;
		$total = 0;
		if (is_dir($dir)) 
		{
			if ($handle = opendir($dir)) 
			{
				while (($file = readdir($handle)) !== false)
				{
					if (!in_array($file, mainClassConfig::$ExceptFiles)) 
					{
						if (in_array('f', $tips) && is_file($dir.$file))
						{
							$total++;
						}
						if (in_array('d', $tips) && is_dir($dir.$file))
						{
							$total++;
						}
					}
				}
				closedir($handle);
			}
		}
		return $total;
	}
}