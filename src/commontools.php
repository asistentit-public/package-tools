<?
namespace asistentit\tools;

use \Ramsey\Uuid\Uuid;

class commontools
{
    public static function getuuid()
    {
        $uuid4 = Uuid::uuid4();
        return $uuid4->toString();
    }
}